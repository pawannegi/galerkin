
# coding: utf-8

# In[10]:


import numpy as np
import time


# In[12]:


def mymatrix(N):
    A = np.zeros((N,N))
    for i in range(N):
        A[i][i] = 4
        if(i > 0):
           A[i][i-1] = -1.
        if (i < N-1):
            A[i][i+1] = -1.
        if (i > 2):
            A[i][i-3] = -1
        if (i < N-3):
            A[i][i+3] = -1
            
    A[3,2] = 0
    A[6,5] = 0.
    
    return A

def bmat(N):
    b = np.ones(N)
    b[0] = 4
    b[1] = -1
    b[2] = -5
    b[3] = -2
    b[4] = 2
    b[5] = 2
    b[6] = -1
    b[7] = 1
    b[8] = 6
    
    return b
    
 
def soln(N):
    b = np.ones(N)
    b[0] = 1
    b[1] = 0
    b[2] = -1
    b[3] = 0
    b[4] = 1
    b[5] = 1
    b[6] = 0
    b[7] = 1
    b[8] = 2
    
    return b
    
def gausselemination(N = 9):
    A = mymatrix(N)
    b = bmat(N)
    x = np.zeros(N)
    # forward elemination
    
    for i in range(0,N):
        for j in range(i+1,N):
            m = A[j][i]/A[i][i]
            A[j] = A[j] - m * A[i]
            b[j] = b[j] - m * b[i]
            
    # back substitution
    for i in range(N,0,-1):
        mysum = 0
        for j in range(N,i ,-1):
            mysum = mysum + A[i-1][j-1] / A[i-1][i-1] * x[j-1]
        x[i-1] = b[i-1] / A[i-1][i-1] - mysum
    
    return x

def jacobi_iteration(N):
    A = mymatrix(N)
    b = bmat(N)
    exact = soln(N)
    
    p1 , N1 = split_matrix_jacobi(A)
    invN =np.linalg.inv(N1)
    M = np.dot(invN , p1)
    
    err = 10
    sol = np.ones(N) 
    while (err > 0.0001):
        newsol = np.dot(invN , b) - np.dot(M , sol)
        sol = newsol.copy()
        err = max(np.abs(sol - exact))
        
    return sol
    
def seidel_iteration(N):
    A = mymatrix(N)
    b = bmat(N)
    exact = soln(N)
    
    p1 , N1 = split_matrix_seidel(A)
    invN =np.linalg.inv(N1)
    M = np.dot(invN , p1)
    
    err = 10
    sol = np.ones(N) 
    while (err > 0.0001):
        newsol = np.dot(invN , b) - np.dot(M , sol)
        sol = newsol.copy()
        err = max(np.abs(sol - exact))
        
    return sol
    
    
def split_matrix_jacobi(A):
    N_ = A.copy() * 0.0
    p = A.copy() * 0.0
    
    for i in range(len(A)):
        N_[i][i] = A[i][i]
        
    p = A - N_
    
    return p , N_

def split_matrix_seidel(A):
    N_ = A.copy() * 0.0
    p = A.copy() * 0.0
    
    for i in range(len(A)):
        for j in range(len(A)):
            if(j<=i):
                N_[i][j] = A[i][j]
        
    p = A - N_
    
    return p , N_
    
    
    
    
            


# In[19]:


start1 = time.time()
sol1 = seidel_iteration(9)
end1 = time.time()
start2 = time.time()
sol2 = jacobi_iteration(9)
end2 = time.time()
start3 = time.time()
sol3 = gausselemination(9)
end3 = time.time()

print("solution of gauss elimination is" , sol3.round())
print("time taken by gauss elimination is" , end3-start3 , 'secs')
print("solution of gauss siedel is" , sol1.round())
print("time taken by gauss siedel is" , end1-start1 , 'secs')
print("solution of gauss jacobi is" , sol2.round())
print("time taken by gauss jacobi is" , end2-start2 , 'secs')

