
# coding: utf-8

# Assignment -1 submitted by Pawan negi (174010003)
# ==========================

# In[1]:


import numpy as np
from matplotlib import pyplot as plt


# All functions
# =======
# 

# In[2]:


def lagrange_polynomial(index , x , xi=-10000):
    val = 1 ;
    xval = x[index]
    if not (xi == -10000):
        xval = xi
    for i in range (len(x)):
        if not(abs(x[index] - x[i]) < 0.0001 ):
            val = val * (xval - x[i]) / (x[index] - x[i])
    
    return val

def lagrange_polynomial_1dash(index , x , xi=-10000):
    val = 1 ;
    xval = x[index]
    if not (xi == -10000):
        xval = xi
    mysum = 0.0
    dosum = True
    for i in range (len(x)):
        if not(abs(x[index] - x[i]) < 0.001 ):
            if (abs(xval - x[i]) < 0.001):
                val = val * 1.0 / (x[index] - x[i])
                dosum = False 
                mysum = 1.0
                continue
            val = val * (xval - x[i]) / (x[index] - x[i])
            if dosum:
                mysum += 1. / (xval - x[i])
        
    return val * mysum

def lagrange_polynomial_2dash(index , x , xi=-10000):
    pass

def my_func(x):
    return 1 / (1 + np.cos(np.pi / 2 * x))

def my_func_dash(x):
    return (np.pi/2 * np.sin(np.pi / 2 * x)) / (1 + np.cos(np.pi / 2 * x))**2

def interpolate_equispaced(N ,func ,  isplot = True):
    x = np.linspace(-1.,1. , N)
    y = func(x)
    
    xnew = np.linspace(-1.,1. , N*2)
    ynew = xnew.copy() * 0.0
    
    for i in range(len(xnew)):
        for j in range(len(x)):
            ynew[i] += lagrange_polynomial(j , x , xnew[i]) * y[j]
            
    if (isplot):
        plt.plot(x,y , '-o')
        plt.plot(xnew,ynew)
        plt.legend(['exact' , 'interpolated']) 
        
    return L2_Norm(xnew , ynew , my_func)  

def interpolate_equispaced_1dash(N ,func ,funcdash,  isplot = True):
    x = np.linspace(-1.,1. , N)
    y = func(x)
    ydash = funcdash(x)
    
    xnew = np.linspace(-1.,1. , N*2)
    ynew = xnew.copy() * 0.0
    
    for i in range(len(xnew)):
        for j in range(len(x)):
            ynew[i] += lagrange_polynomial_1dash(j , x , xnew[i]) * y[j]
            
    if (isplot):
        plt.plot(x,ydash , '-o')
        plt.plot(xnew[1:-2],ynew[1:-2])
        plt.legend(['exact' , 'interpolated']) 
        
    return L2_Norm(xnew[1:-2] , ynew[1:-2] , funcdash)

def lglpoints(points):
    N = points - 1
    N1=N+1;
    theta = np.linspace(0,np.pi,N1)
    x=np.cos(theta)
    P=np.zeros((N1,N1))
    xold=2
    while (max(abs(x-xold))>0.0001) :
        xold=x;
        P[:,0]=1;    
        P[:,1]=x;
        for k in range(2,N1):
            P[:,k]= ((2*k-1.)*x*P[:,k-1]-(k-1.)*P[:,k-2])/k
        
        x=xold - ( x*P[:,N]-P[:,N-1] )/( N*P[:,N] )  
    
    w=2./(N*N1*P[:,N]**2)
    x = x[::-1]
    w = w[::-1]
    
    return x , w

def lgpoints(points):
    N = points
    k = np.linspace(1,N , N)
    x = (1. - 1./8/N**2 + 1./8/N**3)* np.cos(np.pi * (4 * k - 1)/(4 * N + 2))
    N1=N+1;

    P=np.zeros((N,N1))
    P[:,0]=1;    
    P[:,1]=x;
    for k in range(2,N1):
        P[:,k]= ((2*k-1.)*x*P[:,k-1]-(k-1.)*P[:,k-2])/k

    w = 2.* (1.-x**2)/(N**2 * P[:,N-1]**2)
    
    return x , w

def interpolation_lgl(N , func , isplot= True):
    x , w = lglpoints(N)
    y = func(x)

    xnew = np.linspace(-1.,1. , N*2)
    ynew = xnew.copy() * 0.0
    
    for i in range(len(xnew)):
        for j in range(len(x)):
            ynew[i] += lagrange_polynomial(j , x , xnew[i]) * y[j]
            
    if (isplot):
        plt.plot(x,y , '-o')
        plt.plot(xnew,ynew)
        plt.legend(['exact' , 'interpolated']) 
        
    return L2_Norm(xnew , ynew , my_func) 

def interpolation_lgl_1dash(N , func ,funcdash, isplot= True):
    x , w = lglpoints(N)
    y = func(x)
    ydash = funcdash(x)
    
    xnew = np.linspace(-1.,1. , N*2)
    ynew = xnew.copy() * 0.0
    
    for i in range(len(xnew)):
        for j in range(len(x)):
            ynew[i] += lagrange_polynomial_1dash(j , x , xnew[i]) * y[j]
            
    if (isplot):
        plt.plot(x,ydash , '-o')
        plt.plot(xnew[1:-2],ynew[1:-2])
        plt.legend(['exact' , 'interpolated']) 
        
    return L2_Norm(xnew[1:-2] , ynew[1:-2] , funcdash) 
     
def interpolation_lg(N , func , isplot = True):
    x , w = lgpoints(N)
    y = func(x)

    xnew = np.linspace(-1.,1. , N*2)
    ynew = xnew.copy() * 0.0
    
    for i in range(len(xnew)):
        for j in range(len(x)):
            ynew[i] += lagrange_polynomial(j , x , xnew[i]) * y[j]
       
    if (isplot):
        plt.plot(x,y , '-o')
        plt.plot(xnew,ynew)
        plt.legend(['exact' , 'interpolated']) 
    return L2_Norm(xnew , ynew , my_func)
    

def interpolation_lg_1dash(N , func ,funcdash, isplot= True):
    x , w = lgpoints(N)
    y = func(x)
    ydash = funcdash(x)
    
    xnew = np.linspace(-1.,1. , N*2)
    ynew = xnew.copy() * 0.0
    
    for i in range(len(xnew)):
        for j in range(len(x)):
            ynew[i] += lagrange_polynomial_1dash(j , x , xnew[i]) * y[j]
            
    if (isplot):
        plt.plot(x,ydash , '-o')
        plt.plot(xnew[1:-2],ynew[1:-2])
        plt.legend(['exact' , 'interpolated']) 
        
    return L2_Norm(xnew[1:-2] , ynew[1:-2] , funcdash) 

def L2_Norm(x , y , func):
    yexact = func(x)
    return np.sqrt(sum((yexact - y)**2))/np.sqrt(sum((yexact)**2))

def lgl_integration(N , func):
    exact = 1.27323954473516
    x , w = lglpoints(N)
    y = func(x)

    mysum = 0.0
    for i in range(len(x)):
        val = 0.0
        for j in range(len(x)):
            val += lagrange_polynomial(j , x , x[i])
        mysum += val * y[i] * w[i]
        
    error = abs(exact - mysum) / exact
    return mysum , error

def lg_integration(N , func):
    exact = 1.27323954473516
    x , w = lgpoints(N)
    y = func(x)

    mysum = 0.0
    for i in range(len(x)):
        val = 0.0
        for j in range(len(x)):
            val += lagrange_polynomial(j , x , x[i])
        mysum += val * y[i] * w[i]
        
    error = abs(exact - mysum) / exact
    return mysum , error


