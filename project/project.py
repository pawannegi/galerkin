from interpolation import *


def get_local_mass_matrix(x , exact = False):
    order = len(x)
    M = np.zeros((order , order))
    for i in range(order):
        for j in range(order):
            func1 = lagrange_polynomial
            func2 = lagrange_polynomial
            if not exact:
                M[i,j] = intergration(func1 , func2, i , j , order , x)
            else:
                M[i,j] = intergration(func1 , func2 ,i ,j, order + 1, x)
    return M


def get_local_diff_matrix(x, exact = False):
    order = len(x)
    D = np.zeros((order , order))
    for i in range(order):
        for j in range(order):
            func1 = lagrange_polynomial
            func2 = lagrange_polynomial_1dash
            if not exact:
                D[i,j] = intergration(func1 , func2, i , j , order , x)
            else:
                D[i,j] = intergration(func1 , func2 ,i ,j, order + 1, x)
    return D


def get_local_diff_matrix_DG(x, exact = False):
    order = len(x)
    D = np.zeros((order , order))
    for i in range(order):
        for j in range(order):
            func1 = lagrange_polynomial_1dash
            func2 = lagrange_polynomial
            if not exact:
                D[i,j] = intergration(func1 , func2, i , j , order , x)
            else:
                D[i,j] = intergration(func1 , func2 ,i ,j, order + 1, x)
    return D

def get_local_flux_matrix(x):
    order = len(x)
    F = np.zeros((order , order))
    for i in range(order):
        for j in range(order):
            func1 = lagrange_polynomial
            func2 = lagrange_polynomial
            F[i,j] = get_value_at_boundary(func1 , func2, i , j , x)
    return F

def get_value_at_boundary(func1 , func2 , i , j, x1):
    a =  func1(i , x1 , 1.0) * func2(j , x1 , 1.0)
    b =  func1(i , x1 , -1.0) * func2(j , x1 , -1.0)
    return a - b 

def intergration(func1 , func2 , i , j , N , x1):
    x , w = lglpoints(N)
    val = 0.0
    for k in range(len(x)):
        val += func1(i , x1 , x[k]) * func2(j , x1 , x[k]) * w[k]
    
    return val

def get_jacobian(nodes , i, order):
    canonical_x, w = lglpoints(order)
    J = 0.0
    index = i * (order - 1)
    for k in range (order):
        J += lagrange_polynomial_1dash(k , canonical_x ,
                            canonical_x[0]) * nodes[index]
        index += 1
    return abs(J)
    
def create_mesh(N , order):
    x = np.linspace(0 , 1 , N)
    canonical, w = lglpoints(order)

    N_c = (N- 1) * (order - 1) + 1

    nodes = np.zeros(N_c)
    index = 0
    nodes[0] = x[index]

    for i in range(N - 1):
        for j in range (1 , order):
            index += 1
            nodes[index] = x[i] - (canonical[j] - canonical[0]) * (x[i] - x[i+1]) /2
    
    nodes[-1] = x[-1]
    return x , nodes

def build_connectivity(N , order):
    connectivity = []
    index = 0 ;
    internal_node = 0 ;
    for i in range(N - 1):
        nodes = []
        nodes.append(index)
        index += 1
        for i in range(order - 2):
            nodes.append(index)
            index += 1
        nodes.append( index)
        connectivity.append(nodes)

    return connectivity

def build_connectivity_matrix(N , order):
    N_c = (N- 1) * (order - 1) + 1
    N_r = (N- 1) * order

    Conn = np.zeros((N_r , N_c))
    colindex = 0 
    index = 0
    for i in range(N_r):
        Conn[i][colindex] = 1.0
        colindex += 1
        index += 1
        if (index > order - 1):
            colindex -= 1
            index = 0

    return Conn

def assemble_local_matrices(m , d , x, N , order , nodes):
    N_r = (N- 1) * order
    M_ = np.zeros((N_r , N_r))
    D_ = np.zeros((N_r , N_r))

    ne = N - 1
    for i in range(ne):
        J = get_jacobian(nodes, i , order)
        M_[order * i:order *(i+1),order * i:order *(i+1)] += m * J
        D_[order * i:order *(i+1),order * i:order *(i+1)] += d
    
    return M_ , D_

def make_global_matrix(N_g , order, N, ele_nodes, all_nodes , exact = True):
    canonical_x , w = lglpoints(order)
    m = get_local_mass_matrix(canonical_x, exact)
    d = get_local_diff_matrix(canonical_x , exact)
    
    C_mat = build_connectivity_matrix(N, order)
    M_ , D_ = assemble_local_matrices(m , d , ele_nodes, N, order , all_nodes)

    M = np.dot(np.dot(C_mat.T , M_), C_mat)
    D = np.dot(np.dot(C_mat.T , D_), C_mat)

    return M , D

def initialize(nodes):
    hs = 0.5 * np.cos(2 * np.pi * nodes)
    U = 0.0 * nodes
    return hs, U

def implement_periodic_bc(M, D , N_g):
    row1 = np.copy(M[0])
    row2 = np.copy(M[N_g - 1])
    M[0] += np.concatenate([[row2[-1]] , row2[0:-1]])
    M[N_g - 1] += np.concatenate([row1[1:] , [row1[0]]])

    row3 = np.copy(D[0])
    row4 = np.copy(D[N_g - 1])
    D[0] += np.concatenate([[row4[-1]] , row4[0:-1]])
    D[N_g - 1] += np.concatenate([row3[1:] , [row3[0]]])

    return M , D

def set_dirchlet_bc(MinvD , N_g):
    MinvD[0] = np.zeros(N_g)
    MinvD[N_g - 1] = np.zeros(N_g)
    MinvD[0,0] = 1.0
    MinvD[N_g -1 , N_g - 1] = 1.0

    return MinvD

def set_neuman_bc(MinvD , N_g):
    MinvD[0] = np.zeros(N_g)
    MinvD[N_g - 1] = np.zeros(N_g)
    MinvD[0,0] = 1.0
    MinvD[0,1] = -1.0
    MinvD[N_g -1 , N_g - 1] = 1.0
    MinvD[N_g -1 , N_g - 2] = -1.0

    return MinvD

def get_exact(nodes , time ):
    hs = 0.5 * np.cos(2 * np.pi * nodes) * np.cos(2 * np.pi * time)
    U = 0.5 * np.sin(2 * np.pi * nodes) * np.sin(2 * np.pi * time)
    return U , hs

def update_boundary(hs , U):
    U[0] = 0
    U[len(U) - 1] = 0
    return hs , U

def rk2_integration(MinvD , hs , U , dt , steps , nodes , doplot):
    hsold = np.copy(hs)
    Uold = np.copy(U)
    dtb2 = dt * 0.5
    time = 0.0
    if (doplot):
        plt.figure() 
    for i in range(steps):
        hsnb2 = hsold + dtb2 * np.dot(MinvD , Uold)
        Unb2 = Uold + dtb2 *  np.dot(MinvD , hsold)
        hsnb2 , Unb2 = update_boundary(hsnb2 , Unb2)
        hsold = hsold + dt * np.dot(MinvD , Unb2)
        Uold = Uold + dt *  np.dot(MinvD , hsnb2)
        hsold , Uold = update_boundary(hsold , Uold)
        time = time + dt
        U , hs = get_exact(nodes , time)
        if (doplot):
            plt.cla() 
            plt.plot(nodes , U , '--')
            plt.plot(nodes , hs, '--')
            plt.plot(nodes , Uold)
            plt.plot(nodes , hsold)
            plt.pause(0.05)

    error1 = np.sqrt(sum((Uold - U)**2))
    error2 = np.sqrt(sum((hsold - hs)**2))
    return error1 , error2


def CG(N , order, exact = True , doplot = False):
    ele_nodes, all_nodes = create_mesh(N , order)
    dx = all_nodes[1] - all_nodes[0]
    hs , U = initialize(all_nodes)
    uhs = 1
    uu = -1
    nn = len(ele_nodes)
    ne = nn - 1
    N_g = len(all_nodes)

    M , D = make_global_matrix(N_g, order , N, ele_nodes , all_nodes , exact)
    D = - D
    Minv = np.linalg.inv(M)
    MinvD = np.dot(Minv , D)

    dt = 10**(-5)
    steps = int(round(0.25/dt))

    res = rk2_integration(MinvD , hs , U , dt , steps , all_nodes , doplot)

    return res[0]

def initialize_DG(all_nodes , conn):
    U = np.zeros_like(conn , dtype = float)
    hs =  np.zeros_like(conn , dtype = float)
    for i in range(len(hs)):
        hs[i] =  0.5 * np.cos(2 * np.pi * all_nodes[conn[i]])
    return hs , U


def solve_local_block(D_tilda , F_tilda , hs , U , hs_int , U_int , ne , dt ,
                      dx):
    hsnew = np.zeros_like(hs)
    Unew = np.zeros_like(U)
    for i in range(ne):
        U_star = rosunov_flux(i ,U_int , hs_int  , ne )
        hs_star = rosunov_flux(i , hs_int , U_int , ne )
        hsnew[i] = hs[i] +  dt * (np.dot(D_tilda , U_int[i]) 
                                - np.dot(F_tilda ,U_star))
        Unew[i] = U[i] + dt * (np.dot(D_tilda , hs_int[i]) 
                                - np.dot(F_tilda ,hs_star))
    return hsnew , Unew

def rosunov_flux(i , f , q , ne):
    fstar = np.zeros_like(f[i], dtype = float)
    if (i == 0):
        fstar[0] = 0.5 * ( f[i][0] + f[i][0] + (q[i][0] - q[i][0]) )
        fstar[-1] = 0.5 * ( f[i][-1] + f[i+1][0] - (q[i+1][0] - q[i][-1]) )
        return fstar
    if (i == ne - 1):
        fstar[0] = 0.5 * ( f[i-1][-1] + f[i][0] + (q[i-1][-1] - q[i][0]) )
        fstar[-1] = 0.5 * ( f[i][-1] + f[i][-1] - (q[i][-1] - q[i][-1]) )
        return fstar

    fstar[0] = 0.5 * ( f[i-1][-1] + f[i][0] + (q[i-1][-1] - q[i][0]) )
    fstar[-1] = 0.5 * ( f[i][-1] + f[i+1][0] - (q[i+1][0] - q[i][-1]) )
    return fstar

def update_boundary_DG(h , U):
    U[0,0] = 0.0
    U[len(h)-1, len(h[0])-1] = 0.0

    return h , U
 
def rk2_integration_DG(nodes , D_tilda , F_tilda , U , hs , ne , dt, steps , dx ,
                      doplot = False):
    hsold = np.copy(hs)
    Uold = np.copy(U)
    dtb2 = dt * 0.5
    time = 0.0
    if (doplot):
        plt.figure() 
    for i in range(steps):
        hsnb2 , Unb2 = solve_local_block(D_tilda , F_tilda , hsold , Uold , hsold ,
                                         Uold , ne , dtb2 , dx)
        hsnb2 , Unb2 = update_boundary_DG(hsnb2 , Unb2)
        hsold , Uold = solve_local_block(D_tilda , F_tilda , hsold , Uold , hsnb2 , Unb2 ,
                                 ne , dt , dx)
        hsold , Uold = update_boundary_DG(hsold , Uold)
        time = time + dt
        if (doplot):
            U , hs = get_exact(nodes , time)
            hsplot =  np.concatenate([[hsold[0,0]] , hsold[:,1:].ravel()])
            Uplot =  np.concatenate([[Uold[0,0]] , Uold[:,1:].ravel()])
            plt.cla() 
            plt.plot(nodes , U , '--')
            plt.plot(nodes , hs, '--')
            plt.plot(nodes , Uplot)
            plt.plot(nodes , hsplot)
            plt.pause(0.05)

    U , hs = get_exact(nodes , time)
    hsplot =  np.concatenate([[hsold[0,0]] , hsold[:,1:].ravel()])
    Uplot =  np.concatenate([[Uold[0,0]] , Uold[:,1:].ravel()])
    error1 = np.sqrt(sum((Uplot - U)**2))
    error2 = np.sqrt(sum((hsplot - hs)**2))
    return error1 , error2


def DG(N , order , exact = True , doplot = False):
    ele_nodes, all_nodes = create_mesh(N , order)
    conn = build_connectivity(N , order)
    dx = all_nodes[1] - all_nodes[0]
    J = (ele_nodes[1] - ele_nodes[0]) / 2
    hs , U = initialize_DG(all_nodes , conn)
    nn = len(ele_nodes)
    ne = nn - 1
    N_g = len(all_nodes)
    
    canonical_x , w = lglpoints(order)
    m = get_local_mass_matrix(canonical_x, exact) * J 
    d = get_local_diff_matrix_DG(canonical_x , exact)
    f = get_local_flux_matrix(canonical_x )

    minv = np.linalg.inv(m)
    D_tilda = np.dot(minv , d)
    F_tilda = np.dot(minv , f)

    dt = 10**(-5)
    steps = int(round(0.25/dt))
    #steps = 10

    res = rk2_integration_DG(all_nodes, D_tilda , F_tilda , U , hs , ne , dt, steps , dx ,
                      doplot)

    return res[0]


def myplot(x , y , order , title):
    plt.figure()
    plt.title(title)
    plt.xlabel("Np")
    plt.ylabel("L2 Norm")
    plt.grid()
    plt.plot(x , y)
    plt.legend(['%d order'%(order)])
    plt.savefig(title + '.pdf')

def simulation(doCG = True, doexact = True , doplot = True):
    if (doCG & doexact):
        title1 = 'CG exact order = '
    if not(doCG & doexact):
        title1 = 'DG inexact order = '
    if ( doCG &  (not doexact)):
        title1 = 'CG inexact order = '
    if ((not doCG) & doexact):
        title1 = 'DG exact order = '

    order = 1
    elements = [32, 64 , 96 , 128]
    error = []
    Np = []
    for ele in elements:
        if(doCG):
            err = CG(ele + 1 , order + 1 , doexact , False )
        else:
            err = DG(ele + 1 , order + 1 , doexact , False )
        error.append(err)
        Np.append(order * ele + 1)
    title = title1 + '%d'%(order)
    myplot(Np , error , order , title)
    
    
    order = 2
    elements = [16, 32 , 48 , 64]
    error = []
    Np = []
    for ele in elements:
        if(doCG):
            err = CG(ele + 1 , order + 1 , doexact , False )
        else:
            err = DG(ele + 1 , order + 1 , doexact , False )
        error.append(err)
        Np.append(order * ele + 1)
    title = title1 + '%d'%(order)
    myplot(Np , error , order , title)
    
    order = 4
    elements = [8, 16 , 24, 32]
    error = []
    Np = []
    for ele in elements:
        if(doCG):
            err = CG(ele + 1 , order + 1 , doexact , False )
        else:
            err = DG(ele + 1 , order + 1 , doexact , False )
        error.append(err)
        Np.append(order * ele + 1)
    title = title1 + '%d'%(order)
    myplot(Np , error , order , title)
    
    order = 8
    elements = [4 , 8 , 12 , 16]
    error = []
    Np = []
    for ele in elements:
        if(doCG):
            err = CG(ele + 1 , order + 1 , doexact , False )
        else:
            err = DG(ele + 1 , order + 1 , doexact , False )
        error.append(err)
        Np.append(order * ele + 1)
    title = title1 + '%d'%(order)
    myplot(Np , error , order , title)
    
    order = 16
    elements = [2, 4 , 6 , 8]
    error = []
    Np = []
    for ele in elements:
        if(doCG):
            err = CG(ele + 1 , order + 1 , doexact , False )
        else:
            err = DG(ele + 1 , order + 1 , doexact , False )
        error.append(err)
        Np.append(order * ele + 1)
    title = title1 + '%d'%(order)
    myplot(Np , error , order , title)
   
    if (doplot):
        plt.show()

if __name__ == "__main__":

    #print DG(16 ,2  , True , False)
    simulation(doCG = True , doexact = True , doplot = False)
    simulation(doCG = True , doexact = False , doplot = False)
    simulation(doCG = False , doexact = True , doplot = False)
    simulation(doCG = False , doexact = False , doplot = False)




