
# coding: utf-8

# In[1]:


import numpy as np


# In[233]:


def mymatrix(N):
    A = np.zeros((N,N))
    for i in range(0,N):
        for j in range(0,N):
            A[i][j] = np.max([i+1,j+1])
            
    return A
    
def bmat(N):
    b = np.ones(N)
    
    return b

def gausselemination(N):
    A = mymatrix(N)
    b = bmat(N)
    x = np.zeros(N)
    # forward elemination
    
    for i in range(0,N):
        for j in range(i+1,N):
            m = A[j][i]/A[i][i]
            A[j] = A[j] - m * A[i]
            b[j] = b[j] - m * b[i]
            
    # back substitution
    for i in range(N,0,-1):
        mysum = 0
        for j in range(N,i ,-1):
            mysum = mysum + A[i-1][j-1] / A[i-1][i-1] * x[j-1]
        x[i-1] = b[i-1] / A[i-1][i-1] - mysum
    
    return x
    
def gausselemination_withpivoting(N):
    A = mymatrix(N)
    b = bmat(N)
    x = np.zeros(N)
    
    # forward elemination
    
    for i in range(0,N):
        dopivot = ispivotingneeded(A , i , i)
        if (dopivot):
            A , b = dopivoting(A , b, dopivot , i)
            
        for j in range(i+1,N):
            m = A[j][i]/A[i][i]
            A[j] = A[j] - m * A[i]
            b[j] = b[j] - m * b[i]
            
    # back substitution
    for i in range(N,0,-1):
        mysum = 0
        for j in range(N,i ,-1):
            mysum = mysum + A[i-1][j-1] / A[i-1][i-1] * x[j-1]
        x[i-1] = b[i-1] / A[i-1][i-1] - mysum
    
    return x
    
def dopivoting(A , b, row1 , row2):
    rownew = A[row1].copy()
    A[row1,:]= A[row2].copy()
    A[row2,:] = rownew.copy()
    
    newa = b[row1]
    b[row1] = b[row2]
    b[row2] = newa
    return A , b
    
def ispivotingneeded(A , colnum , rownum):
    a = A[rownum:,colnum]
    max_index = np.argmax(a)
    
    if (max_index + rownum ==rownum):
        return 0
    else:
        return max_index + rownum
    
