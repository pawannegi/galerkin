
# coding: utf-8

# In[1]:


import numpy as np
import time


# In[30]:


def sparse_mat(N):
    A = np.zeros((N,N))
    for i in range(0,N):
        A[i][i] = 4.
        if(i > 0):
           A[i][i-1] = 1.
        if (i < N-1):
            A[i][i+1] = 1.
    return A
    
def bmat(N):
    b = np.ones(N)
    return b

def lu_decomp(N):
    A = sparse_mat(N)
    b = bmat(N)
    
    a , b , c = extract_abc(A)
    
    alpha , beta = calc_alpha(a , b , c)
    
    x = solving(b , alpha , beta , c)
    
    return x
    
    
def extract_abc(A):
    a = []
    b = []
    c = []
    N = len(A)
    for i in range(N):
        b.append(A[i][i])
        if(i > 0):
           a.append(A[i][i-1])
        if (i < N-1):
            c.append(A[i][i+1])
            
    return np.array(a) , np.array(b) ,np.array(c)

def calc_alpha(a , b , c):
    beta = []
    alpha = []
    
    beta.append(b[0])
    index = 0
    for i in range(len(b) - 1):
        alp = a[i]/beta[index]
        alpha.append(alp)
        bet = b[i] - alpha[index] * c[i]
        index += 1
        beta.append(bet)
        
    return np.array(alpha) , np.array(beta)

def solving(b , alpha , beta , c):
    g = []
    g.append(b[0])
    N = len(b)
    for i in range(N -1):
        newg = b[i] - alpha[i] * g[i]
        g.append(newg)
    
    x = []
    x.append(g[N - 1]/beta[N-1])
    index = 0 ;
    for i in range(N-1,0,-1):
        newx = (g[i] - c[i-1] * x[index])/beta[i]
        index +=1
        x.append(newx)
        
    return x
        


# In[31]:


start = time.time()
x = lu_decomp(1000)
end = time.time()

print 'LU time' , (end - start)

print 'gauss elemination time' , 3.79037189484

