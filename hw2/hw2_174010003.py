from interpolation import *


def get_local_mass_matrix(x , exact = False):
    order = len(x)
    M = np.zeros((order , order))
    for i in range(order):
        for j in range(order):
            func1 = lagrange_polynomial
            func2 = lagrange_polynomial
            if not exact:
                M[i,j] = intergration(func1 , func2, i , j , order , x)
            else:
                M[i,j] = intergration(func1 , func2 ,i ,j, order + 1, x)
    return M


def get_local_diff_matrix(x, exact = False):
    order = len(x)
    D = np.zeros((order , order))
    for i in range(order):
        for j in range(order):
            func1 = lagrange_polynomial
            func2 = lagrange_polynomial_1dash
            if not exact:
                D[i,j] = intergration(func1 , func2, i , j , order , x)
            else:
                D[i,j] = intergration(func1 , func2 ,i ,j, order + 1, x)
    return D


def get_local_diff_matrix_DG(x, exact = False):
    order = len(x)
    D = np.zeros((order , order))
    for i in range(order):
        for j in range(order):
            func1 = lagrange_polynomial_1dash
            func2 = lagrange_polynomial
            if not exact:
                D[i,j] = intergration(func1 , func2, i , j , order , x)
            else:
                D[i,j] = intergration(func1 , func2 ,i ,j, order + 1, x)
    return D

def get_local_flux_matrix(x):
    order = len(x)
    F = np.zeros((order , order))
    for i in range(order):
        for j in range(order):
            func1 = lagrange_polynomial
            func2 = lagrange_polynomial
            F[i,j] = get_value_at_boundary(func1 , func2, i , j , x)
    return F

def get_value_at_boundary(func1 , func2 , i , j, x1):
    a =  func1(i , x1 , 1.0) * func2(j , x1 , 1.0)
    b =  func1(i , x1 , -1.0) * func2(j , x1 , -1.0)
    return a - b 

def intergration(func1 , func2 , i , j , N , x1):
    x , w = lglpoints(N)
    val = 0.0
    for k in range(len(x)):
        val += func1(i , x1 , x[k]) * func2(j , x1 , x[k]) * w[k]
    
    return val

def get_jacobian(nodes , i, order):
    canonical_x, w = lglpoints(order)
    J = 0.0
    index = i * (order - 1)
    for k in range (order):
        J += lagrange_polynomial_1dash(k , canonical_x ,
                            canonical_x[0]) * nodes[index]
        index += 1
    return abs(J)
    
def create_mesh(N , order):
    x = np.linspace(-1 , 1 , N)
    canonical, w = lglpoints(order)

    N_c = (N- 1) * (order - 1) + 1

    nodes = np.zeros(N_c)
    index = 0
    nodes[0] = x[index]

    for i in range(N - 1):
        for j in range (1 , order):
            index += 1
            nodes[index] = x[i] - (canonical[j] - canonical[0]) * (x[i] - x[i+1]) /2
    
    nodes[-1] = x[-1]
    return x , nodes

def build_connectivity(N , order):
    connectivity = []
    index = 0 ;
    internal_node = 0 ;
    for i in range(N - 1):
        nodes = []
        nodes.append(index)
        index += 1
        for i in range(order - 2):
            nodes.append(index)
            index += 1
        nodes.append( index)
        connectivity.append(nodes)

    return connectivity

def build_connectivity_matrix(N , order):
    N_c = (N- 1) * (order - 1) + 1
    N_r = (N- 1) * order

    Conn = np.zeros((N_r , N_c))
    colindex = 0 
    index = 0
    for i in range(N_r):
        Conn[i][colindex] = 1.0
        colindex += 1
        index += 1
        if (index > order - 1):
            colindex -= 1
            index = 0

    return Conn

def assemble_local_matrices(m , d , x, N , order , nodes):
    N_r = (N- 1) * order
    M_ = np.zeros((N_r , N_r))
    D_ = np.zeros((N_r , N_r))

    ne = N - 1
    for i in range(ne):
        J = get_jacobian(nodes, i , order)
        M_[order * i:order *(i+1),order * i:order *(i+1)] += m * J
        D_[order * i:order *(i+1),order * i:order *(i+1)] += d
    
    return M_ , D_

def make_global_matrix(N_g , order, N, ele_nodes, all_nodes , exact = True):
    canonical_x , w = lglpoints(order)
    m = get_local_mass_matrix(canonical_x, exact)
    d = get_local_diff_matrix(canonical_x , exact)
    
    C_mat = build_connectivity_matrix(N, order)
    M_ , D_ = assemble_local_matrices(m , d , ele_nodes, N, order , all_nodes)

    M = np.dot(np.dot(C_mat.T , M_), C_mat)
    D = np.dot(np.dot(C_mat.T , D_), C_mat)

    return M , D

def initialize(nodes):
    sig = 1.0/8
    q = np.exp(- nodes**2 /2 / sig)
    return q

def implement_periodic_bc(M, D , N_g):
    row1 = np.copy(M[0])
    row2 = np.copy(M[N_g - 1])
    M[0] += np.concatenate([[row2[-1]] , row2[0:-1]])
    M[N_g - 1] += np.concatenate([row1[1:] , [row1[0]]])

    row3 = np.copy(D[0])
    row4 = np.copy(D[N_g - 1])
    D[0] += np.concatenate([[row4[-1]] , row4[0:-1]])
    D[N_g - 1] += np.concatenate([row3[1:] , [row3[0]]])

    return M , D

def rk2_integration(MinvD , q , dt , steps , nodes , doplot):
    qold = np.copy(q)
    dtb2 = dt * 0.5
    if (doplot):
        plt.figure() 
    for i in range(steps):
        qnb2 = qold + dtb2 * np.dot(MinvD , qold)
        qold = qold + dt * np.dot(MinvD , qnb2)
        if (doplot):
            plt.cla() 
            plt.plot(nodes , qold)
            plt.pause(0.05)

    return qold


def CG(N , order, exact = True , doplot = False):
    ele_nodes, all_nodes = create_mesh(N , order)
    dx = all_nodes[1] - all_nodes[0]
    q = initialize(all_nodes)
    U = 2.0
    nn = len(ele_nodes)
    ne = nn - 1
    N_g = len(all_nodes)

    M , D = make_global_matrix(N_g, order , N, ele_nodes , all_nodes , exact)
    D = - D * U
    M , D = implement_periodic_bc(M , D , N_g)
    Minv = np.linalg.inv(M)
    MinvD = np.dot(Minv , D)

    dt = 0.25 * dx / U
    steps = int(round(1.0/dt))

    res = rk2_integration(MinvD , q , dt , steps , all_nodes , doplot)
    error = np.sqrt(sum((q - res)**2) / sum(q**2))

    return error

def initialize_DG(all_nodes , conn):
    q = np.zeros_like(conn , dtype = float)
    sig = 1.0/8
    for i in range(len(q)):
        q[i] = np.exp(-1.0 * all_nodes[conn[i]]**2. /2. / sig)
    return q


def solve_local_block(D_tilda , F_tilda , q , flux , ne , dt , dx , U):
    qnew = np.zeros_like(q)
    for i in range(ne):
        fstar = upwind_fstar(i , flux , ne )
        qnew[i] = q[i] +  dt * (np.dot(D_tilda , flux[i]) 
                                - np.dot(F_tilda ,fstar))
    return qnew

def upwind_fstar(i, flux, ne):
    fstar = np.zeros_like(flux[i], dtype = float)
    if (i==0):
        fstar[0] = flux[ne-1][-1]
        fstar[-1] = flux[0][-1]
        return fstar

    fstar[0] = flux[i-1][-1]
    fstar[-1] = flux[i][-1] 
    return fstar
 
def average_fstar(i, flux, ne):
    fstar = np.zeros_like(flux[i] , dtype = float)
    if (i==0):
        fstar[0] = 0.5 * (flux[ne-1][-1] + flux[0][0])
        fstar[-1] = 0.5 * (flux[1][0] + flux[0][-1])
        return fstar
    if (i==ne-1):
        fstar[0] = 0.5 * (flux[ne-1][0] + flux[ne-2][-1])
        fstar[-1] = 0.5 * (flux[0][0] + flux[ne-1][-1])
        return fstar
    fstar[0] = 0.5 * (flux[i-1][-1] + flux[i][0])
    fstar[-1] = 0.5 * (flux[i][-1] + flux[i+1][0])
    return fstar

def rk2_integration_DG(nodes , D_tilda , F_tilda , q , ne, U , dt, steps , dx ,
                      doplot = False):
    qold = np.copy(q)
    dtb2 = dt * 0.5
    if (doplot):
        plt.figure() 
    for i in range(steps):
        flux = U * qold
        qnb2 = solve_local_block(D_tilda , F_tilda , qold , flux , ne , dtb2 ,
                                dx, U)
        flux = U * qnb2 
        qold = solve_local_block(D_tilda , F_tilda , qold , flux , ne , dt, dx,
                                U)
        if (doplot):
            qplot =  np.concatenate([[qold[0,0]] , qold[:,1:].ravel()])
            plt.cla() 
            plt.plot(nodes , qplot)
            plt.pause(0.05)
     
    res =  np.concatenate([[qold[0,0]] , qold[:,1:].ravel()])
    return res


def DG(N , order , exact = True , doplot = False):
    ele_nodes, all_nodes = create_mesh(N , order)
    conn = build_connectivity(N , order)
    dx = all_nodes[1] - all_nodes[0]
    J = (ele_nodes[1] - ele_nodes[0]) / 2
    q = initialize_DG(all_nodes , conn)
    U = 2.0
    nn = len(ele_nodes)
    ne = nn - 1
    N_g = len(all_nodes)
    
    canonical_x , w = lglpoints(order)
    m = get_local_mass_matrix(canonical_x, exact) * J 
    d = get_local_diff_matrix_DG(canonical_x , exact)
    f = get_local_flux_matrix(canonical_x )

    minv = np.linalg.inv(m)
    D_tilda = np.dot(minv , d)
    F_tilda = np.dot(minv , f)

    dt = 0.1 * dx / U
    steps = int(round(1.0/dt))

    res = rk2_integration_DG(all_nodes, D_tilda , F_tilda , q , ne, U , dt, steps ,
                       dx , doplot)

    q0 =  np.concatenate([[q[0,0]] , q[:,1:].ravel()])
    error = np.sqrt(sum((q0 - res)**2) / sum(q0**2))

    return error


def myplot(x , y , order , title):
    plt.figure()
    plt.title(title)
    plt.xlabel("Np")
    plt.ylabel("L2 Norm")
    plt.grid()
    plt.plot(x , y)
    plt.legend(['%d order'%(order)])
    plt.savefig(title + '.pdf')

def simulation(doCG = True, doexact = True , doplot = True):
    if (doCG & doexact):
        title1 = 'CG exact order = '
    if not(doCG & doexact):
        title1 = 'DG inexact order = '
    if ( doCG &  (not doexact)):
        title1 = 'CG inexact order = '
    if ((not doCG) & doexact):
        title1 = 'DG exact order = '

    order = 1
    elements = [16, 32 , 64]
    error = []
    Np = []
    for ele in elements:
        if(doCG):
            err = CG(ele + 1 , order + 1 , doexact , False )
        else:
            err = DG(ele + 1 , order + 1 , doexact , False )
        error.append(err)
        Np.append(order * ele + 1)
    title = title1 + '%d'%(order)
    myplot(Np , error , order , title)
    
    
    order = 4
    elements = [4, 8 , 16]
    error = []
    Np = []
    for ele in elements:
        if(doCG):
            err = CG(ele + 1 , order + 1 , doexact , False )
        else:
            err = DG(ele + 1 , order + 1 , doexact , False )
        error.append(err)
        Np.append(order * ele + 1)
    title = title1 + '%d'%(order)
    myplot(Np , error , order , title)
    
    order = 8
    elements = [2, 4 , 8]
    error = []
    Np = []
    for ele in elements:
        if(doCG):
            err = CG(ele + 1 , order + 1 , doexact , False )
        else:
            err = DG(ele + 1 , order + 1 , doexact , False )
        error.append(err)
        Np.append(order * ele + 1)
    title = title1 + '%d'%(order)
    myplot(Np , error , order , title)
    
    order = 16
    elements = [1, 2 , 4]
    error = []
    Np = []
    for ele in elements:
        if(doCG):
            err = CG(ele + 1 , order + 1 , doexact , False )
        else:
            err = DG(ele + 1 , order + 1 , doexact , False )
        error.append(err)
        Np.append(order * ele + 1)
    title = title1 + '%d'%(order)
    myplot(Np , error , order , title)
   
    if (doplot):
        plt.show()

if __name__ == "__main__":

    simulation(doCG = True , doexact = True , doplot = False)
    simulation(doCG = True , doexact = False , doplot = False)
    simulation(doCG = False , doexact = True , doplot = False)
    simulation(doCG = False , doexact = False , doplot = False)




